import configparser
import logging
from mongodb import MongoDB
from telegram.ext import Updater, CommandHandler, CallbackContext
from telegram import Update

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

# Get info
config = configparser.ConfigParser()
config.read('config.ini')

user = config['mongo atlas']['user']
password = config['mongo atlas']['password']
cluster = config['mongo atlas']['cluster']
database = config['mongo atlas']['database']
collection = config['mongo atlas']['collection']
db = MongoDB(user, password, cluster, database, collection)



# ------------------------------- Command Handlers Functions ---------------------------------------

def start(update: Update, context: CallbackContext):
    """Send a message when the command /start is issued."""
    update.message.reply_text(
        'Hello!, I am spy bot and I can tell you who is connected at home.'
        ' Type /help for more information')


def help_bot(update: Update, context: CallbackContext):
    """Send a message when the command /help is issued."""
    update.message.reply_text('You can use the following commands:\n'
                              'If you want to know who is connected at home type: /check\n'
                              'To add a family member type /add <family member> <IP address>\n'
                              'To delete a family member type: /delete <family member>\n'
                              'To change the name type: /change_name  <old name> <new name>\n'
                              'To change the IP address type: /change_ipadd  <name> <new IP address>\n'
                              )

def check_whoshome(update, context):
    """Checks who is connected at Home's router."""
    context.bot.send_message(chat_id=update.message.chat_id, text=db.ping_all())


def add_familymember(update: Update, context: CallbackContext):
    response = context.args
    print(response)
    if len(response) <= 1:
        text = "Error. Few arguments. Type /add <name> <IP address> "
        context.bot.send_message(chat_id=update.message.chat_id, text=text)
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text=db.add_member(response[0], response[1]))


def delete_familymember(update: Update, context: CallbackContext):
    response = context.args
    print(response)
    if not response:
        text = "Error. Few arguments. Type /delete <name>"
        context.bot.send_message(chat_id=update.message.chat_id, text=text)
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text=db.delete_member(response[0]))


def change_name(update: Update, context: CallbackContext):
    response = context.args
    print(response)
    if len(response) <= 1:
        text = "Error. Few arguments. Type /change_name <old name> <new name>"
        context.bot.send_message(chat_id=update.message.chat_id, text=text)
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text=db.edit_name(response[0], response[1]))


def change_ipaddress(update: Update, context: CallbackContext):
    response = context.args
    print(response)
    if len(response) <= 1:
        text = "Error. Few arguments. Type /change_ipadd <name> <new IP address>"
        context.bot.send_message(chat_id=update.message.chat_id, text=text)
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text=db.edit_ipaddress(response[0], response[1]))

# -------------------------------------------- Main function -------------------------------------------

def main():
    # Get Token from config.ini file
    token = config['telegram.org']['Token']

    # Create the Updater and pass it bot's token.
    updater = Updater(token=token, use_context=True)
    dispatcher = updater.dispatcher

    # regex
    regex = r"(.+)([,: ])(192.168.1.(2([0-5][0-3])|1([0-9][0-9])|[7-9][0-9]|6[0-9]))"

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("check", check_whoshome))
    dispatcher.add_handler(CommandHandler("add", add_familymember))
    dispatcher.add_handler(CommandHandler("change_name", change_name))
    dispatcher.add_handler(CommandHandler("change_ipadd", change_ipaddress))
    dispatcher.add_handler(CommandHandler("delete", delete_familymember))
    dispatcher.add_handler(CommandHandler("help", help_bot))

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()