FROM balenalib/raspberrypi3-debian-python:3.5.7
ENTRYPOINT []
WORKDIR /whoshome_telegram
ADD . /whoshome_telegram
RUN install_packages gcc libc-dev libc6-dev build-essential libssl-dev libffi-dev   && pip install --upgrade pip  && pip install -r requirements.txt
CMD ["python", "whoshome_bot.py"]

