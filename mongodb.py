from pymongo import MongoClient, errors
from os import system


# import configparser


class MongoDB:
    def __init__(self, user, password, cluster, db, collection):

        # Get MongoDB info from config.ini file
        self.user = user
        self.password = password
        self.cluster = cluster
        self.db = db
        self.collectionName = collection
        self._client = self.__connect()
        self._collection = self.__get_collection()

    def __connect(self):
        try:
            return MongoClient("mongodb+srv://{}:{}@{}/".format(self.user, self.password, self.cluster))

        except errors.ConnectionFailure as e:
            return print("Could not connect to server1: %s" % e)
        except errors.ConfigurationError as e:
            return print("Could not connect to server2: %s" % e)
        except errors.ServerSelectionTimeoutError as e:
            return print("Could not connect to server3: %s." % e)

    def __get_collection(self):

        if self._client is not None:
            db = self._client[self.db]
            return db[self.collectionName]

    def __find_all(self):
        if self._collection is not None:
            print('Get all users: ')
            pipeline = [{"$project": {"_id": 0, "name": 1, "ip_address": 1}}]
            return self._collection.aggregate(pipeline)

    def ping_all(self):
        if self.__find_all() is not None:
            print('Ping all users: ')
            family = self.__find_all()
            responses = list()
            for member in family:
                response = system("ping -c 1 " + member['ip_address'])
                if response == 0:
                    responses.append(member['name'] + " is connected")
                else:
                    responses.append(member['name'] + " is not connected")
            return "\n".join(responses).join(['\n', '\n'])

    def add_member(self, name, ipaddress):
        if self._collection is not None:
            if name in self._collection.distinct('name'):
                return name + " is already added"
            elif ipaddress in self._collection.distinct('ip_address'):
                return ipaddress + " is already taken.  Choose another"
            else:
                try:
                    self._collection.insert_one({"name": name, "ip_address": ipaddress})
                    return "{} {} was added".format(name, ipaddress)
                except errors.OperationFailure as e:
                    print(e)

    def edit_name(self, old_name, new_name):
        if self._collection is not None:
            if old_name in self._collection.distinct('name'):
                if new_name in self._collection.distinct('name'):
                    return "{} is being used. Choose another user name.".format(new_name)
                else:
                    result = self._collection.update_one({"name": old_name}, {"$set": {"name": new_name}})
                    if result.modified_count > 0:
                        return "{} was changed to {}".format(old_name, new_name)
            else:
                return "User {} does not exist. Verify name.".format(old_name)


    def edit_ipaddress(self, name, ipdaddress):
        if self._collection is not None:
            if ipdaddress in self._collection.distinct('ip_address'):
                return "{} is already taken. Choose another".format(ipdaddress)
            elif name not in self._collection.distinct('name'):
                return "User {} does not exist. Verify name.".format(name)
            else:
                old_ipaddress = self._collection.find_one({"name": name})
                result = self._collection.update_one({"name": name}, {"$set": {"ip_address": ipdaddress}})
                if result.modified_count > 0:
                    return "{} was changed to {}".format(old_ipaddress['ip_address'], ipdaddress)

    def delete_member(self, name):
        if self._collection is not None:
            if name in self._collection.distinct('name'):
                d = self._collection.delete_one({"name": name})
                if d.deleted_count > 0:
                    return "{} was deleted".format(name)
            else:
                return "User {} does not exist. Verify name.".format(name)

# config = configparser.ConfigParser()
# config.read('config.ini')

# Get MongoDB info from config.ini file
# user = config['mongo atlas']['user']
# password = config['mongo atlas']['password']
# cluster = config['mongo atlas']['cluster']
# database = config['mongo atlas']['database']
# collection = config['mongo atlas']['collection']

# mongoDatabase = MongoDB(user, password, cluster, database, collection)

# print(mongodb.ping_all())
# print(mongodb.add_member('Momo', '192.168.1.90'))
# print(mongodb.delete_member('Momo'))
# print(mongodb.edit_name('Momi', 'Momo'))
# print(mongodb.edit_ipaddress('Momi', '192.168.1.92'))
