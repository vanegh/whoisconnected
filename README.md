# The who is connected Telegram bot

This is a project that develops a **Telegram bot** that tells you who is connected at home's router. 
The bot is written in **Python 3.5.7** with aid of the library 
**[python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot)** (see whoshome_bot.py).
The users are stored in a database, in this case MongoDB. All the methods to do queries can be found in mongodb.py

The python script is ran under a **Docker** container in a **Raspberry pi 3** computer.


Below you can find some steps needed to run the Telegram bot.

---

## How to run the bot

### Raspberry pi 3

  * To log in to the raspberry pi via SSH:
  
    *$ ssh pi@<IP address>*

### Docker

  * To build the image:

    *$ docker build -t vangh/whoshome .*
    

  * To run the container:

    *$ docker run -d vangh/whoshome*
    
### Python library versions

 * pymongo 3.11.0
 * python-telegram-bot   12.8
    
---

## How the Telegram bot works

To accesss to the bot find the **@wathome_bot** username in **Telegram** application.

### Bot Commands

As all bots, the user should start the conversation. So type 

#### /start

A message is displayed with the following information:
![start_bot](images/start_bot.png)
    
For further information on how to use the bot, type 

#### /help
![help_bot](images/help_bot.png)
    
Want to know who connected at home is, type
#### /check
![check_bot](images/check_bot.png)

To add a new member    
#### /add <name> <ip address>
![add_user_bot](images/add_user_bot.png)

The user no longer lives there, type
#### /delete <name>
![delete_bot](images/delete_bot.png)

To change the user name
#### /change_name <old name> <new name>
![rename_user_bot](images/rename_user_bot.png)

To change the ip address of a user
#### /change_ipadd <name> <new IP address>
![change_ipadd_bot](images/change_ipadd_bot.png)


    
    
  




